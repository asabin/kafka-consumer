package com.example.kafka.consumer;

import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.kafka.annotation.EnableKafka;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.annotation.TopicPartition;

@EnableKafka
@SpringBootApplication
public class KafkaConsumerApplication {

    private final static String TOPIC_1 = "test-kafka-topic-1";
    private final static String TOPIC_2 = "test-kafka-topic-2";

    private final static String PARTITION_0 = "0";
    private final static String PARTITION_1 = "1";

    private static final String GROUP_1 = "group-1";
    private static final String CONSUMER_1 = "consumer-1";
    private static final String CONSUMER_2 = "consumer-2";

    private static final String GROUP_2 = "group-2";
    private static final String CONSUMER_3 = "consumer-3";

    public static void main(String[] args) {
        SpringApplication.run(KafkaConsumerApplication.class, args);
    }

    @KafkaListener(topics = TOPIC_1)
    public void msgListener(String msg) {
        System.out.println("Get msg: " + msg);
    }

    @KafkaListener(topics = TOPIC_2, groupId = GROUP_1, topicPartitions = {
            @TopicPartition(topic = TOPIC_2, partitions = {PARTITION_0})
    })
    public void msgListener1(ConsumerRecord<String, String> record) {
        System.out.println(getResult(GROUP_1, CONSUMER_1, record));
    }

    @KafkaListener(topics = TOPIC_2, groupId = GROUP_1, topicPartitions = {
            @TopicPartition(topic = TOPIC_2, partitions = {PARTITION_1})
    })
    public void msgListener2(ConsumerRecord<String, String> record) {
        System.out.println(getResult(GROUP_1, CONSUMER_2, record));
    }

    @KafkaListener(topics = TOPIC_2, groupId = GROUP_2, topicPartitions = {
            @TopicPartition(topic = TOPIC_2, partitions = {PARTITION_0, "1"})
    })
    public void msgListener3(ConsumerRecord<String, String> record) {
        System.out.println(getResult(GROUP_2, CONSUMER_3, record));
    }

    private String getResult(String group, String consumer, ConsumerRecord<String, String> record) {
        return String.join(" : ",
                group,
                consumer,
                record.topic(),
                "partition-" + record.partition(),
                "offset = " + record.offset(),
                record.key(),
                record.value());
    }
}
